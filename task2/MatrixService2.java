package task2;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MatrixService2 {

    ExecutorService executor;
    Future future;

    public int sum(int[][]matrix, int nThreads){
        int sum = 0;
        executor = Executors.newFixedThreadPool(nThreads);

        for (int i=0; i<matrix.length; i++) {
            ColumnSummator2 columnSummator = new ColumnSummator2(matrix[i]);
            future = executor.submit(columnSummator);

            try {
                System.out.println("Сумма "+i+" колонки = " + future.get());
                sum += (int)future.get();
            }
            catch (InterruptedException|ExecutionException e) {
                e.printStackTrace();
            }
        }
        return sum;
    }


    public static void main(String[]args){
        int[][] array = {{1,1,1}, {2,2,2}, {3,3,3}};
        MatrixService2 matrixService = new MatrixService2();
        System.out.println("Сумма элементов массива = " + matrixService.sum(array,3));
    }
}
