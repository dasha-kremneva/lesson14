package task2;

import java.util.concurrent.Callable;

public class ColumnSummator2 implements Callable<Integer> {

    public int [] arr;

    public ColumnSummator2(int [] arr){
        this.arr = arr;
    }

    @Override
    public Integer call() throws Exception{
        int sum = 0;
        for(int i=0; i<arr.length; i++){
            sum += arr[i];
        }
       return sum;
    }
}
