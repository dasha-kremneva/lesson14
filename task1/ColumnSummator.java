package task1;

public class ColumnSummator implements Runnable{

    public int [] arr;
    int sumColumn;

    public ColumnSummator(int [] arr){
        this.arr = arr;
        sumColumn = 0;
    }

    //задача суммирует элементы матрицы в определенном диапазоне столбцов
    @Override
    public void run() {
        int sum = 0;
        for(int i=0; i<arr.length; i++){
            sum += arr[i];
        }
        sumColumn = sum;
    }

    public int getSumColumn(){
        this.run();
        System.out.println("Сумма столбца в ColumnSummator.getSumColumn() = " + sumColumn);
        return sumColumn;
    }

}
