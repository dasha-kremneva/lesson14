package task1;

import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool{

    private final LinkedBlockingQueue queue;
    Thread thread;
    int sum;

    public FixedThreadPool(int nThreads){
        queue = new LinkedBlockingQueue();
        for (int i = 0; i < nThreads; i++)
            start();
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
    public int getSum() {
        return sum;
    }

    @Override
    public void start() {
        thread = new Thread(() -> {
            ColumnSummator task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Произошла ошибка во время ожидания очереди: " + e.getMessage());
                        }
                    }
                    task = (ColumnSummator) queue.poll();
                }
                try {
                    sum = task.getSumColumn();
                    //System.out.println("Сумма FixedThreadPool.sum = " + sum);
                } catch (RuntimeException e) {
                    System.out.println("Пул потоков прерван из-за ошибки: " + e.getMessage());
                }
            }
        });

        thread.start();
    }

    @Override
    public void execute(ColumnSummator task) {
        synchronized (queue){
            queue.add(task);
            queue.notify();
        }
    }
}
