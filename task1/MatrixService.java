package task1;

public class MatrixService {

  FixedThreadPool pool;

    int sum(int[][]matrix, int nThreads){
        int sum = 0;
        pool = new FixedThreadPool(nThreads);

        for (int i=0; i<matrix.length; i++){
            pool.execute(new ColumnSummator(matrix[i]));

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            sum += pool.getSum();

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        return sum;
    }

    public static void main(String[]args){
        int[][] array = {{1,1,1}, {2,2,2}, {3,3,3}};              // сумма массива   (3+6+9) = 18
        MatrixService matrixService = new MatrixService();
        System.out.println("Сумма элементов массива = " + matrixService.sum(array,3));
    }
}
